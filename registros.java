import java.text.DecimalFormat;
import java.util.ArrayList;

public class registros {
	ArrayList<custos> registros = new ArrayList();
	DecimalFormat df = new DecimalFormat("#.00");

	
	 public void adicionarEstadia (registros r1, String pessoa, int numeroDeDias, float valorEstadiaPorDia) {
		 r1.registros.add(new custos("Estadia", valorEstadiaPorDia*numeroDeDias, pessoa));
	 }
	 
	 public void adicionarLigacaoTelefonica (registros r1, String pessoa, int numeroDeLicacoes, float valorLigacao) {
		 r1.registros.add(new custos("Ligação Telefônica", numeroDeLicacoes*valorLigacao, pessoa));
	 }

	 public void adicionarRefeicao (registros r1, String pessoa, int numeroRefeicoes, float valorRefeicao) {
		 r1.registros.add(new custos("Refeição", numeroRefeicoes*valorRefeicao, pessoa));
	 }
	
	 public void CustoTotaldaEstadia(registros r1, String pessoa) {
	
		int i;
		int n = r1.registros.size();
		String pessoa1 = pessoa;
		float valorTotal = 0;
				
	    for (i=0; i<n; i++) {
	    	if(r1.registros.get(i).pessoa == pessoa1) {
	    		valorTotal = valorTotal + r1.registros.get(i).valor;
	    	}
	    }
	    
	    System.out.printf("O valor total da estadia de " + pessoa + " foi: " + df.format(valorTotal*1.05) + "\n");
	}

	
}