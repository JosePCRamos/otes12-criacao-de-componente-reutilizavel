**Elaborar um componente reutilizável da seguinte problemática:**

Carol Stefany ficou duas noites no West Country Vista Hotel. O valor da estadia era de R$125 por noite. Carol fez uma ligação telefônica que custou R$ 4,50 e pediu uma refeição no apartamento por R$ 66,00. As contas de hotel incluem um imposto estadual de 5% sobre os serviços.

---

## Importar arquivos

1. Crie o main.java
2. Inclua os dois arquivos custos.java e registros.java dentro do mesmo package do java que estiver a main.java
3. Crie uma variável de registro com o seguinte comando:

		registros r1 = new registros();

4. Faça o lançamento dos custos com os métodos adicionarEstadia, adicionarLigacaoTelefonica e adicionarRefeicao, conforme abaixo:

		r1.adicionarEstadia (variável de registros, pessoa que está na estadia, numero de dias, valor da estadia por dia (em float))
		r1.adicionarLigacaoTelefonica (variável de registros, pessoa que está na estadia, numero ligações, valor da ligação (em float))
		r1.adicionarRefeicao (variável de registros, pessoa que está na estadia, numero refeições, valor da refeição (em float))

		Exemplos:

		r1.adicionarEstadia(r1, "Ana", 2, 125);
		r1.adicionarLigacaoTelefonica(r1, "Ana", 1, (float) 4.5);
		r1.adicionarRefeicao(r1, "Ana", 1, 66);

5. Para calcular o valor total de uma estadia, utilize o método CustoTotaldaEstadia, conforme abaixo:

		r1.CustoTotaldaEstadia(variável de registros, nome da pessoa)

		Exemplo:

		r1.CustoTotaldaEstadia(r1, "Ana");