public class main {

	public static void main(String[] args) {
		
		//Criação um registro geral
		registros r1 = new registros();
		
		//Lançamento dos custos
		//r1.adicionarEstadia (variável de registros, pessoa que está na estadia, numero de dias, valor da estadia por dia (em float))
		//r1.adicionarLigacaoTelefonica (variável de registros, pessoa que está na estadia, numero ligações, valor da ligação (em float))
		//r1.adicionarRefeicao (variável de registros, pessoa que está na estadia, numero refeições, valor da refeição (em float))
		
		r1.adicionarEstadia(r1, "Ana", 2, 125);
		r1.adicionarLigacaoTelefonica(r1, "Ana", 1, (float) 4.5);
		r1.adicionarRefeicao(r1, "Ana", 1, 66);
				
		//Imprime o valor total da estadia
		//r1.CustoTotaldaEstadia(variável de registros, nome da pessoa)
		r1.CustoTotaldaEstadia(r1, "Ana");
	}

}
